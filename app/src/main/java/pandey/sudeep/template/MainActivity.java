package pandey.sudeep.template;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private HorizontalScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scrollView = (HorizontalScrollView)findViewById(R.id.scroll);

        TextView titleDisplay = (TextView)findViewById(R.id.title);
        titleDisplay.setText(BuildConfig.MainTitle);

        List<TextView> textViewContainer = new ArrayList<TextView>();
        textViewContainer.add((TextView)findViewById(R.id.title));
        textViewContainer.add((TextView) findViewById(R.id.version));
        textViewContainer.add((TextView) findViewById(R.id.fragment));
        textViewContainer.add((TextView) findViewById(R.id.service));
        textViewContainer.add((TextView) findViewById(R.id.broadcast));
        textViewContainer.add((TextView) findViewById(R.id.jobscheduler));
        textViewContainer.add((TextView) findViewById(R.id.multithreading));
        textViewContainer.add((TextView) findViewById(R.id.loader));
        textViewContainer.add((TextView) findViewById(R.id.recyclerView));
        textViewContainer.add((TextView) findViewById(R.id.persistence));
        textViewContainer.add((TextView) findViewById(R.id.provider));
        textViewContainer.add((TextView) findViewById(R.id.alarms));
        textViewContainer.add((TextView) findViewById(R.id.material));

        for (int i=0;i<textViewContainer.size();i++){
            UIUtils.autoSizeText(textViewContainer.get(i));
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray("ARTICLE_SCROLL_POSITION",
                new int[]{ scrollView.getScrollX(), scrollView.getScrollY()});
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int[] position = savedInstanceState.getIntArray("ARTICLE_SCROLL_POSITION");
        if(position != null)
            scrollView.post(new Runnable() {
                public void run() {
                    scrollView.scrollTo(position[0], position[1]);
                }
            });
    }

}
