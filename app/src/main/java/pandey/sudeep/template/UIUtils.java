package pandey.sudeep.template;

import android.support.v4.widget.TextViewCompat;
import android.widget.TextView;

import static android.support.v4.widget.TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM;

public class UIUtils {

    public static void autoSizeText(TextView textView){
        TextViewCompat.setAutoSizeTextTypeWithDefaults(textView, AUTO_SIZE_TEXT_TYPE_UNIFORM);
    }
}
